import React from "react";
import "./About.css";
import theme_pattern from "../../assets/theme_pattern.svg";
import profile_img from "../../assets/mypic.jpeg";

const About = () => {
  return (
    <div id="about" className="about">
      <div className="about-title">
        <h1>About Me</h1>
        <img src={theme_pattern} alt="Theme Pattern" />
      </div>

      <div className="about-section">
        <div className="about-left">
          <img src={profile_img} alt="Profile" />
        </div>

        <div className="about-right">
          <div className="about-para">
            <p>
              I am an experienced Frontend Developer with over 2.5 years of experience. I have developed responsive and interactive web applications, created frontend dashboards for app management, adhering to coding guidelines and best practices. I have also improved application functionalities by implementing new features and integrated third-party APIs and services to enhance application functionality and user experience.
            </p>
          </div>

          <div className="about-skills">
            <div className="about-skill">
              <p>HTML & CSS</p>
              <hr style={{ width: "80%" }} />
            </div>
            <div className="about-skill">
              <p>REACT JS</p>
              <hr style={{ width: "70%" }} />
            </div>
            <div className="about-skill">
              <p>JAVASCRIPT</p>
              <hr style={{ width: "50%" }} />
            </div>
            <div className="about-skill">
              <p>GIT</p>
              <hr style={{ width: "80%" }} />
            </div>
          </div>
        </div>
      </div>

      <div className="about-achievements">
        <div className="about-achievement">
          <h1>2+</h1>
          <p>Years of Experience</p>
        </div>
        <div className="about-achievement">
          <h1>30+</h1>
          <p>Projects Completed</p>
        </div>
        <div className="about-achievement">
          <h1>1+</h1>
          <p>Live Projects</p>
        </div>
      </div>
    </div>
  );
};

export default About;
