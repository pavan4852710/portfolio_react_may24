import React from 'react'
import "./Contact.css"
import theme_pattern from "../../assets/theme_pattern.svg"
import mail_icon from "../../assets/mail_icon.svg"
import location_icon from "../../assets/location_icon.svg"
import call_icon from "../../assets/call_icon.svg"

const Contact = () => {


    const [result, setResult] = React.useState("");

  const onSubmit = async (event) => {
    event.preventDefault();
    setResult("Sending....");
    const formData = new FormData(event.target);

    formData.append("access_key", "4dee32e9-a32c-4bd2-93a2-e729303313f1");

    const response = await fetch("https://api.web3forms.com/submit", {
      method: "POST",
      body: formData
    });

    const data = await response.json();

    if (data.success) {
      alert("Form Submitted Successfully")
      setResult("Form Submitted Successfully");
      event.target.reset();
    } else {
      console.log("Error", data);
      setResult(data.message);
    }
}

  return (
    <div className='contact' id='contact'>
        <div className="contact-title">
            <h1>Get in Touch</h1>
            <img src={theme_pattern} alt="" />
        </div>

        <div className="contact-section">
            <div className="contact-left">
                <h1>Let's Talk</h1>
                <p>Feel free to connect with me regarding job oppurtunities</p>
                
                <div className="contact-details">
                    <div className="contact-detail">
                        <img src={mail_icon} alt="" />
                        <p>pavansai3012@gmail.com</p>
                    </div>
                    <div className="contact-detail">
                        <img src={call_icon} alt="" />
                        <p>939241841</p>
                    </div>
                    <div className="contact-detail">
                       <img src={location_icon} alt="" />
                       <p>Hyderabad,India</p>
                    </div>
                </div>


            </div>
            <form onSubmit={onSubmit} className="contact-right">

                <label htmlFor="">Your Name</label>
                <input type="text" placeholder='Enter Your Name' name='name' />

                <label htmlFor="">Your Email</label>
                <input type="email" placeholder='Enter Your Email' name='email' />

                <label htmlFor="">Write Your Message Here</label>
                <textarea name="message" rows="8" id="" placeholder='Enter message'></textarea>
                
                <button type='submit' className='contact-submit'>SUBMIT</button>
            </form>
        </div>
    </div>
  )
}

export default Contact