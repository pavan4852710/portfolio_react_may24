import React from 'react';
import "./Services.css";
import theme_pattern from "../../assets/theme_pattern.svg";
import mywork_data from "../../assets/mywork_data.js";
import arrow_icon from "../../assets/arrow_icon.svg";
import { FiGithub } from "react-icons/fi";

const Services = () => {
  return (
    <div id='services' className='mywork'>
      <div className="mywork-title">
        <h1>My Projects</h1>
        <img src={theme_pattern} alt="Theme Pattern" />
      </div>

      <div className="mywork-container">
        {mywork_data.map((work, index) => (
          <a href={work.w_url} target='__blank' key={index}>
            <img src={work.w_img} alt={work.w_name} />
            <p>{work.w_name} <br /><FiGithub /></p>
          </a>
        ))}
      </div>

      <div className="mywork-showmore">
        <p>Show More</p>
        <img src={arrow_icon} alt="Arrow Icon" />
      </div>
    </div>
  );
}

export default Services;
