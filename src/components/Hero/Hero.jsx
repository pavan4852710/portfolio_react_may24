import React from 'react';
import "./Hero.css";
import profilepic from "../../assets/PIC_ZOOM.jpeg";
import AnchorLink from 'react-anchor-link-smooth-scroll';

const Hero = () => {
  return (
    <div id='home' className='hero'>
        <img src={profilepic} alt="Profile" />
        <h1>I'm <span>Pavan Sai</span>, A Web Developer</h1>
        <p>I am a frontend developer from Hyderabad, India with 2.5 years of experience in the IT field.</p>
        <div className="hero-action">
            <div className="hero-connect">
                <AnchorLink className='anchor-link' offset={50} href='#contact'>Connect with me</AnchorLink>
            </div>
            <div className="hero-resume">My Resume</div>   
        </div>
    </div>
  );
}

export default Hero;
