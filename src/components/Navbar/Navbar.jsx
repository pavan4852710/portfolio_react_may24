import React, { useRef, useState } from 'react'
import "./Navbar.css"
//import logo from "../../assets/logo.svg"
//import logo from "../../assets/logo2_pavan.png"
import logo from "../../assets/logo_pavan.png"
import underline from "../../assets/nav_underline.svg"
import AnchorLink from 'react-anchor-link-smooth-scroll'
import menu_open from "../../assets/menu_open.svg"
import menu_close from "../../assets/menu_close.svg"


const Navbar = () => {

  const[menu,setMenu]=useState("about")
  const menuRef=useRef()


  const openMenu=()=>{
    menuRef.current.style.right='0'
  }

  const closeMenu=()=>{
    menuRef.current.style.right='-350px'
  }

  return (
    <div className='navbar'>
        {/** */}<img className='logo' src={logo} alt=''/>
        <img onClick={openMenu} src={menu_open} alt="" className='nav-mob-open'/>
        <ul ref={menuRef} className='nav-menu'>
        <img onClick={closeMenu} src={menu_close} alt="" className="nav-mob-close" />
            <li><AnchorLink className='anchore-link' href='#home'><p onClick={()=>setMenu("home")}>Home</p></AnchorLink>
            {menu==="home"?<img src={underline} alt='' />:<></>}
            </li>
            <li><AnchorLink  className='anchore-link' offset={50} href='#about'><p onClick={()=>setMenu("about")}>About me</p></AnchorLink>
            {menu==="about"?<img src={underline} alt='' />:<></>}</li>
            <li><AnchorLink  className='anchore-link' offset={50} href='#services'><p onClick={()=>setMenu("services")}>Projects</p></AnchorLink>
            {menu==="services"?<img src={underline} alt='' />:<></>}</li>
            <li><AnchorLink  className='anchore-link' offset={50} href='#contact'><p onClick={()=>setMenu("contact")}>Contact</p></AnchorLink>
            {menu==="contact"?<img src={underline} alt='' />:<></>}</li>
        </ul>
        <div className="nav-connect"><AnchorLink  className='anchore-link' offset={50} href='#contact'>Connect with me</AnchorLink></div>
    </div>
  )
}

export default Navbar