import project1_img from '../assets/project_1.svg'
import project2_img from '../assets/project_2.svg'
import project3_img from '../assets/project_3.svg'
import project4_img from '../assets/project_4.svg'
import project5_img from '../assets/project_5.svg'
import project6_img from '../assets/project_6.svg'
import linked from "../assets/linked.png"
import food from "../assets/food.png"
import gemini from "../assets/gemini.png"
import tailwind from "../assets/tailwind.png"
import spotify from "../assets/spotify.png"
import clg from "../assets/clg.png"
import health from "../assets/health.png"

const mywork_data = [
    
  
    {
        w_no:2,
        w_name:"Spotify Like app with rich ui",
        w_img:spotify,
        w_url:"https://spotify-clone-react24.netlify.app/"
    },
    {
        w_no:1,
        w_name:"Gemini ChatGpt Clone",
        w_img:gemini,
        w_url:"https://chatgpt-app24.netlify.app/"
    },
    {
        w_no:9,
        w_name:"Advanced Landing page tailwind",
        w_img:tailwind,
        w_url:"https://gleeful-blini-8bbbf5.netlify.app/"
    },
    {
        w_no:8,
        w_name:"LinkedIn Clone",
        w_img:linked,
        w_url:"https://linkedin-clone-24.netlify.app/"
    },
    {
        w_no:4,
        w_name:"College website",
        w_img:clg,
        w_url:"https://college-website-react.netlify.app/"
    },
    {
        w_no:7,
        w_name:"Food Delivery app",
        w_img:food,
        w_url:"https://app.netlify.com/sites/online-food-app24/overview"
    },
    {
        w_no:5,
        w_name:"Health Facts Blog",
        w_img:health,
        w_url:"https://health-blog-a0n4qwn8a-t-pavan-sais-projects.vercel.app/"
    },
    {
        w_no:11,
        w_name:"Youtube Clone",
        w_img:project2_img,
        w_url:"https://gitlab.com/pavan4852710/youtube_clone_react"
    },
    {
        w_no:3,
        w_name:"shoppingcart redux toolkit",
        w_img:project3_img,
        w_url:"https://gitlab.com/pavan4852710/shoppingcart_redux_toolkit"
    },
    {
        w_no:12,
        w_name:"Simple Chat App",
        w_img:project4_img,
        w_url:"https://gitlab.com/pavan4852710/chatapp_mern_socket.io"
    },
    {
        w_no:12,
        w_name:"Ecommerce UI with HTML+CSS",
        w_img:project5_img,
        w_url:"https://gitlab.com/pavan4852710/shopping_ui_html_css"
    },
    {
        w_no:6,
        w_name:"Event Planner-MERN",
        w_img:project6_img,
        w_url:"https://gitlab.com/pavan4852710/eventplanner_app_mern"
    },
]
 
export default mywork_data;